package com.pligor.fortumoandroid

import android.app.Activity
import android.os.Bundle
import com.fortumo.android.{PaymentResponse, PaymentRequestBuilder, Fortumo}
import android.content.Intent
import com.pligor.myandroid.AndroidHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * http://developers.fortumo.com/in-app-purchasing-on-android/configuration/
 *
 * INCLUDE THESE PERMISSIONS INSIDE MANIFEST
 * DO NOT FORGET TO REPLACE com.your.domain with your own package name
<!-- Permissions -->
  <uses-permission android:name="android.permission.INTERNET" />
  <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
  <uses-permission android:name="android.permission.READ_PHONE_STATE" />
  <uses-permission android:name="android.permission.RECEIVE_SMS" />
  <uses-permission android:name="android.permission.SEND_SMS" />
  <!-- Define your own permission to protect payment broadcast -->
  <permission android:label="Read Fortumo payment status" android:name="com.your.domain.PAYMENT_BROADCAST_PERMISSION" android:protectionlevel="signature"></permission>
  <!-- "signature" permission granted automatically by system, without notifying user. -->
  <uses-permission android:name="com.your.domain.PAYMENT_BROADCAST_PERMISSION">
  </uses-permission>
 *
 * INCLUDE THE BELOW INSIDE application section in AndroidManifest.xml
 <!-- Declare these objects, this is part of Fortumo SDK, and should not be called directly -->
  <receiver android:name="com.fortumo.android.BillingSMSReceiver">
  <intent-filter>
    <action android:name="android.provider.Telephony.SMS_RECEIVED"></action>
  </intent-filter>
  </receiver>
  <service android:name="com.fortumo.android.FortumoService" />
  <service android:name="com.fortumo.android.StatusUpdateService" />
  <!--To support Mobile Payments for Web fallback for devices without a SIM card-->
  <activity
          android:configChanges="orientation|keyboardHidden|screenSize"
          android:name="com.fortumo.android.FortumoActivity"
          android:theme="@android:style/Theme.Translucent.NoTitleBar"/>
 *
 *
 * IMPLEMENT A BROADCAST RECEIVER
 * DO NOT FORGET TO REPLACE com.your.domain with your own package name
 <!-- Implement you own BroadcastReceiver to track payment status, should be protected by "signature" permission -->
  <receiver android:name="receivers.FortumoPaymentStatusReceiver"
            android:permission="com.your.domain.PAYMENT_BROADCAST_PERMISSION">
      <intent-filter>
          <action android:name="com.fortumo.android.PAYMENT_STATUS_CHANGED"></action>
      </intent-filter>
  </receiver>
 *
 * INSIDE PROGUARD CONFIGURATION YOU HAVE TO ADD THESE TWO LINES
  -keep class com.fortumo.** { *; }
  -dontwarn plugin.fortumo.**
 *
 */
trait FortumoPaymentActivity extends Activity {
  //abstract

  //take that directly from manifest setup, its unique for each project
  protected def FORTUMO_PERMISSION: String

  protected def FORTUMO_REQUEST_CODE: Int

  protected def onFortumoBillingStatus(paymentResponse: PaymentResponse): Unit

  protected def onFortumoNotOkResult(resultCode: Int): Unit

  protected def fortumo_message_status_billed_id: Int

  protected def fortumo_message_status_pending_id: Int

  protected def fortumo_message_status_not_sent_id: Int

  protected def fortumo_message_status_failed_id: Int

  protected def fortumo_message_status_use_alternative_method_id: Int

  protected def fortumo_result_code_not_ok_id: Int

  //concrete

  override def onPostCreate(savedInstanceState: Bundle): Unit = {
    super.onPostCreate(savedInstanceState)

    Fortumo.enablePaymentBroadcast(this, FORTUMO_PERMISSION)
  }

  protected def makeFortumoPayment(paymentRequestBuilder: PaymentRequestBuilder): Unit = {
    startActivityForResult(paymentRequestBuilder.build().toIntent(this), FORTUMO_REQUEST_CODE)
  }

  override def onActivityResult(requestCode: Int, resultCode: Int, data: Intent): Unit = {
    implicit val localContext = this

    if (requestCode == FORTUMO_REQUEST_CODE) {
      resultCode match {
        case Activity.RESULT_OK =>
          val response = new PaymentResponse(data)

          val billingStatus = response.getBillingStatus

          billingStatus match {
            case Fortumo.MESSAGE_STATUS_BILLED => showToast(fortumo_message_status_billed_id)
            case Fortumo.MESSAGE_STATUS_PENDING => showToast(fortumo_message_status_pending_id)
            case Fortumo.MESSAGE_STATUS_NOT_SENT => showToast(fortumo_message_status_not_sent_id)
            case Fortumo.MESSAGE_STATUS_FAILED => showToast(fortumo_message_status_failed_id)
            case Fortumo.MESSAGE_STATUS_USE_ALTERNATIVE_METHOD => showToast(fortumo_message_status_use_alternative_method_id)
          }

          onFortumoBillingStatus(response)

        case x =>
          showToast(fortumo_result_code_not_ok_id)

          onFortumoNotOkResult(x)
      }
    }
    else {
      super.onActivityResult(requestCode, resultCode, data)
    }
  }
}
