package com.pligor.fortumoandroid

import play.api.libs.json.Json
import org.apache.http.HttpStatus
import com.pligor.myandroid.{JsonRequestTrait, log}
import com.bugsense.trace.BugSenseHandler
import org.codehaus.jackson.JsonParseException
import java.net.URL
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait FortumoReceiptChecker extends JsonRequestTrait {
  //abstract

  //concrete

  private val connTimeout = 5.seconds

  private val readTimeout = 5.seconds

  /**
   * typically called inside on stop of activity
   */
  protected def cancelFortumoCheck(): Unit = {
    log log "try to finish fortumo check in case it is already running"

    cancellorOption.map(_.apply())

    cancellorOption = None
  }

  private var cancellorOption: Option[() => Boolean] = None

  //we have chosen to let it run in the background until is finished even if it means that
  //next activity will not have catch up

  private class FortumoSubscriptionRefreshException(msg: String) extends Exception(msg)

  protected def checkFortumo(fortumoTimestampUrl: URL,
                             fortumoServiceId: String,
                             fortumoProductId: String,
                             fortumoDeviceReceiptOption: Option[FortumoDeviceReceipt],
                             onNotFoundFortumoReceipt: => Unit,
                             onValidFortumoReceipt: Option[FortumoJsonResponse] => Unit
                              )(op: => Unit)(implicit context: Context, executionContext: ExecutionContext): Unit = {

    //execute only if we have a device receipt in hand, otherwise we cannot do it
    if (fortumoDeviceReceiptOption.isDefined) {

      val fortumoJsonRequest = fortumoDeviceReceiptOption.get.getFortumoJsonRequest(
        fortumoServiceId = fortumoServiceId,
        productName = fortumoProductId
      )

      cancellorOption = Some(
        doJsonRequest(JsonRequestParam(
          fortumoTimestampUrl,
          Json.toJson(fortumoJsonRequest)(FortumoJsonRequest.itsJsonFormat),
          connTimeout_msec = connTimeout.toMillis.toInt,
          readTimeout_msec = readTimeout.toMillis.toInt
        ))({
          jsonResponseOption =>
            if (jsonResponseOption.isDefined) {
              val jsonResponse = jsonResponseOption.get

              jsonResponse.code match {
                case HttpStatus.SC_BAD_REQUEST =>
                  log log "fortumo check bad request"
                  BugSenseHandler.sendException(
                    new FortumoSubscriptionRefreshException("BAD_REQUEST")
                  )

                  op

                case HttpStatus.SC_EXPECTATION_FAILED =>
                  log log "fortumo check expectation failed"
                  BugSenseHandler.sendException(
                    new FortumoSubscriptionRefreshException("EXPECTATION_FAILED")
                  )

                  op

                case HttpStatus.SC_NO_CONTENT =>
                  log log "did not find any fortumo receipt on the server"

                  onNotFoundFortumoReceipt

                  op

                case HttpStatus.SC_OK =>
                  log log "the fortumo response body is: " + jsonResponse.body

                  val fortumoJsonResponseOption = try {
                    Json.fromJson(
                      Json.parse(jsonResponse.body)
                    )(FortumoJsonResponse.itsJsonFormat).fold(invalid = {
                      x => None
                    }, valid = {
                      fortumoJsonResponse =>
                        Some(fortumoJsonResponse)
                    })
                  } catch {
                    case e: JsonParseException => None
                  }

                  if (fortumoJsonResponseOption.isDefined) {
                    log log "got a valid fortumo json receipt"

                    onValidFortumoReceipt(fortumoJsonResponseOption)

                    op
                  } else {
                    log log "got an unknown json response, leave everything as it is"
                    BugSenseHandler.sendException(
                      new FortumoSubscriptionRefreshException("UNKNOWN_JSON_RESPONSE")
                    )

                    op
                  }

                case _ =>
                  log log "fortumo check unexpected server response"
                  BugSenseHandler.sendException(
                    new FortumoSubscriptionRefreshException("UNEXPECTED_SERVER_RESPONSE")
                  )

                  op
              }
            } else {
              log log "on server communication error do not do anything"

              op
            }

            cancellorOption = None
        })
      )
    } else {
      log log "device receipt is not available"

      op
    }
  }
}
