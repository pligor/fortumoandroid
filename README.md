Fortumo Android will help you handle all the necessary steps of fortumo payments in Android.

It is actually a wrapper for fortumo android sdk.

###Dependencies###
* com.fortumo.android
* play.api.libs.json
* com.pligor.mycrypto
* com.pligor.myandroid
* com.bugsense
* org.apache.http

Used in the following projects:

* [bman](http://bman.co)
* [fcarrier](http://facebook.com/FcarrierApp)