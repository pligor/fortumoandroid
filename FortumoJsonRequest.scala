package com.pligor.fortumoandroid

import play.api.libs.json.Json

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object FortumoJsonRequest {
  implicit val itsJsonFormat = Json.format[FortumoJsonRequest]
}

case class FortumoJsonRequest(payment_code: String,
                              user_id: String,
                              service_id: String,
                              product_name: String)
