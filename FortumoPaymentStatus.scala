package com.pligor.fortumoandroid

import android.os.Bundle

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * do NOT use fortumos payment response class, use this one inside the receiver
 */
object FortumoPaymentStatus {

  //there is also the value "sku" which might have to ask support (again) to get an answer
  //of what is this. We get a value of null on a test, so it is definately optional

  def createFromBundle(bundle: Bundle) = FortumoPaymentStatus(
    billing_status = Option(bundle.getString("billing_status")).map(_.toInt),
    credit_amount = Option(bundle.getString("credit_amount")),
    credit_name = Option(bundle.getString("credit_name")),
    message_id = Option(bundle.getString("message_id")).map(_.toInt),
    payment_code = Option(bundle.getString("payment_code")),
    price_amount = Option(bundle.getString("price_amount")).map(_.toFloat),
    price_currency = Option(bundle.getString("price_currency")),
    product_name = Option(bundle.getString("product_name")),
    service_id = Option(bundle.getString("service_id")),
    user_id = Option(bundle.getString("user_id"))
  )
}

case class FortumoPaymentStatus(billing_status: Option[Int], //2
                                credit_amount: Option[String], //depends
                                credit_name: Option[String], //depends
                                message_id: Option[Int], //7 (unfortunately not the same message id we get on the server)
                                payment_code: Option[String], //1384964543801a7
                                price_amount: Option[Float], //1.23
                                price_currency: Option[String], //EUR
                                product_name: Option[String], //unmanaged_subscription
                                service_id: Option[String], //cd2643d4f9de4f68d208717f90106756
                                user_id: Option[String] //202052962324699
                                 ) {

  def getFortumoDeviceReceipt = {
    if (payment_code.isDefined && user_id.isDefined)
      Some(
        FortumoDeviceReceipt(
          payment_code = payment_code.get,
          user_id = user_id.get
        )
      )
    else None
  }

}
