package com.pligor.fortumoandroid

import java.util.Date
import play.api.libs.json._

object FortumoJsonResponse {
  implicit val itsJsonFormat = Json.format[FortumoJsonResponse]
}

case class FortumoJsonResponse(fortumo_receipt_received: Date) {

}

