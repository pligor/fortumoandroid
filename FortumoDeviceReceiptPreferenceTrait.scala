package com.pligor.fortumoandroid

import components.MySecurePreference
import android.content.Context
import com.pligor.mycrypto.crypto.AES
import play.api.libs.json.Json
import org.codehaus.jackson.JsonParseException
import com.pligor.mycrypto.protocol.defaults._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait FortumoDeviceReceiptPreferenceTrait extends MySecurePreference[String, Option[FortumoDeviceReceipt]] {
  protected def secret: String

  val defaultValue: String = null

  def setValue(newValue: Option[FortumoDeviceReceipt])(implicit context: Context): Boolean = {
    setInnerValue(
      if (newValue.isDefined)
        AES.encryptToString(
          Json.toJson(newValue.get)(FortumoDeviceReceipt.itsJsonFormat).toString(),
          secret
        )
      else defaultValue
    )
  }

  def getValue(implicit context: Context): Option[FortumoDeviceReceipt] = {
    Option(getInnerValue.asInstanceOf[String]).flatMap {
      encryptedString =>
        try {
          Json.fromJson(
            Json.parse(AES.decryptFromString(encryptedString, secret))
          )(FortumoDeviceReceipt.itsJsonFormat).fold(invalid = {
            x => None
          }, valid = Some.apply)
        } catch {
          case e: JsonParseException => None
        }
    }
  }
}
