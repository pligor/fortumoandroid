package com.pligor.fortumoandroid

import com.fortumo.android.PaymentResponse
import play.api.libs.json.Json

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object FortumoDeviceReceipt {

  implicit val itsJsonFormat = Json.format[FortumoDeviceReceipt]

  def createFromPaymentResponse(paymentResponse: PaymentResponse): Option[FortumoDeviceReceipt] = {
    val paymentCodeOption = Option(paymentResponse.getPaymentCode)

    val userIdOption = Option(paymentResponse.getUserId)

    if (paymentCodeOption.isDefined && userIdOption.isDefined)
      Some(
        FortumoDeviceReceipt(
          payment_code = paymentCodeOption.get,
          user_id = userIdOption.get
        )
      )
    else None
  }
}

case class FortumoDeviceReceipt(payment_code: String, user_id: String) {
  def getFortumoJsonRequest(fortumoServiceId: String,
                            productName: String) = FortumoJsonRequest(
    payment_code = payment_code,
    user_id = user_id,
    service_id = fortumoServiceId,
    product_name = productName
  )
}

/*fortumoServiceId = SubscriptionProduct.FORTUMO_SERVICE_ID,
productName = SubscriptionProduct.productId*/
