package com.pligor.fortumoandroid

import android.content.Context
import play.api.libs.json.Json
import org.codehaus.jackson.JsonParseException
import com.pligor.myandroid.sqlite.MyDbPreferencesCase

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait FortumoDeviceReceiptDbPreferenceTrait extends MyDbPreferencesCase[String, Option[FortumoDeviceReceipt]] {
  val defaultValue: String = null

  def setValue(newValue: Option[FortumoDeviceReceipt])(implicit context: Context): Boolean = {
    setInnerValue(
      if (newValue.isDefined) {
        Json.toJson(newValue.get)(FortumoDeviceReceipt.itsJsonFormat).toString()
      } else {
        defaultValue
      }
    )
  }

  def getValue(implicit context: Context): Option[FortumoDeviceReceipt] = {
    Option(getInnerValue.asInstanceOf[String]).flatMap {
      str =>
        try {
          Json.fromJson(
            Json.parse(str)
          )(FortumoDeviceReceipt.itsJsonFormat).asOpt
        } catch {
          case e: JsonParseException => None
        }
    }
  }
}
